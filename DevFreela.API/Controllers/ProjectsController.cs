﻿using DevFreela.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace DevFreela.API.Controllers
{
    [Route("api/projects")]
    public class ProjectsController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get(string query)
        {
            // Buscar todos;

            return Ok();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            // Buscar todos ou filtrar

            // return NotFound();

            return Ok();
        }

        [HttpPost]
        public IActionResult Post([FromBody] CreateProjectModel model)
        {
            if (model.Title.Length > 50)
            {
                return BadRequest();
            }

            // Cadastra o projeto

            return CreatedAtAction(nameof(GetById), new { id = model.Id }, model);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] UpdateProjectModel model)
        {
            if (model.Description.Length > 200)
            {
                return BadRequest();
            }

            // Atualiza o projeto

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id) 
        {
            // Buscar, se nao existir retornar NotFound();
            //Remover

            return BadRequest();
        }
    }
}
